import {
    AGREGAR_TIENDA,
    AGREGAR_TIENDA_EXITO,
    AGREGAR_TIENDA_ERROR,
    COMENZAR_DESCARGA_TIENDAS,
    DESCARGA_TIENDAS_EXITO,
    DESCARGA_TIENDAS_ERROR
} from '../types';

// import {
//     AGREGAR_TIENDA,
//     AGREGAR_TIENDA_EXITO,
//     AGREGAR_TIENDA_ERROR,
//     COMENZAR_DESCARGA_PRODUCTOS,
//     DESCARGA_PRODUCTOS_EXITO,
//     DESCARGA_PRODUCTOS_ERROR,
//     OBTENER_PRODUCTO_ELIMINAR,
//     PRODUCTO_ELIMINADO_EXITO,
//     PRODUCTO_ELIMINADO_ERROR,
//     OBTENER_PRODUCTO_EDITAR,
//     PRODUCTO_EDITADO_EXITO,
//     PRODUCTO_EDITADO_ERROR
// } from '../types';

// cada reducer tiene su propio state
const initialState = {
    tiendas: [],
    error: null,
    loading: false, 
}

export default function(state = initialState, action) {
    switch(action.type) {
        case COMENZAR_DESCARGA_TIENDAS:
        case AGREGAR_TIENDA: 
            return {
                ...state,
                loading: action.payload
            }
        case AGREGAR_TIENDA_EXITO:
            return {
                ...state,
                loading: false,
                tiendas: [...state.tiendas, action.payload]
            }
        case DESCARGA_TIENDAS_EXITO:
            return {
                ...state,
                loading: false,
                error: null,
                productos: action.payload
            }    
        case AGREGAR_TIENDA_ERROR:
        case DESCARGA_TIENDAS_ERROR:    
        default:
            return state;
    }
}