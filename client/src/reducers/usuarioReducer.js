import {
    REGISTRO_USUARIO,
    REGISTRO_USUARIO_EXITO,
    REGISTRO_USUARIO_ERROR,
} from '../types';

// cada reducer tiene su propio state
const initialState = {
    usuario: [],
    error: null,
    loading: false
}

export default function(state = initialState, action) {
    switch(action.type) {
        case REGISTRO_USUARIO:
            return {
                ...state,
                loading: action.payload
            }
        case REGISTRO_USUARIO_EXITO:
            return {
                ...state,
                loading: false,
                usuario: [...state.usuario, action.payload]
            }       
        case REGISTRO_USUARIO_ERROR:  
        default:
            return state;
    }
}