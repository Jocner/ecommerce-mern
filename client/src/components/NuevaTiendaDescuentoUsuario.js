import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// Actions de Redux
import { crearTiendaUsuarioAction } from '../actions/procesosActions';
import { mostrarAlerta, ocultarAlertaAction } from '../actions/alertaActions';

const NuevaTiendaUsuario = ({history}) => {

    // state del componente
    const [email, guardarEmail] = useState('');
    const [tienda, guardarTienda] = useState('');
    const [monto, guardarMonto] = useState('');

    // utilizar use dispatch y te crea una función
    const dispatch = useDispatch();

    // Acceder al state del store
    const cargando = useSelector( state => state.tiendas.loading );
    const error = useSelector(state => state.tiendas.error);
    const alerta = useSelector(state => state.alerta.alerta);


    // mandar llamar el action de procesosAction
    const agregarTienda = tienda => dispatch( crearTiendaUsuarioAction(tienda) );

    // cuando el usuario haga submit
    const submitTiendaUsuario = e => {
        e.preventDefault();

        // validar formulario
        if(email.trim() === '' || tienda.trim() === '' || monto.trim() === '' ) {

            const alerta = {
                msg: 'Todos los campos son obligatorios',
                classes: 'alert alert-danger text-center text-uppercase p3'
            }
            dispatch( mostrarAlerta(alerta) );

            return;
        }

        // si no hay errores
        dispatch( ocultarAlertaAction() );

        // crear las nueva data de la tienda
        agregarTienda({
            email,
            tienda,
            monto
        });

        // redireccionar
        history.push('/');
    }


    return ( 
        <div className="row justify-content-center">
            <div className="col-md-8">
                <div className="card">
                    <div className="card-body">
                        <h2 className="text-center mb-4 font-weight-bold">
                            Registro de Tienda y Usuario asociado
                        </h2>

                        {alerta ? <p className={alerta.classes}> {alerta.msg} </p> : null }

                        <form
                            onSubmit={submitTiendaUsuario}
                        >
                            <div className="form-group">
                                <label>Tienda</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Categoria Tienda"
                                    name="tienda"
                                    value={tienda}
                                    onChange={e => guardarTienda(e.target.value)}
                                />
                            </div>

                            <div className="form-group">
                                <label>Email del Usuario</label>
                                <input
                                    type="email"
                                    className="form-control"
                                    placeholder="Email del usuario asociado a la tienda"
                                    name="email"
                                    value={email}
                                    onChange={e => guardarEmail(e.target.value)}
                                />
                            </div>

                            <div className="form-group">
                                <label>Descuento</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Asignar monto de descuento"
                                    name="monto"
                                    value={monto}
                                    onChange={e => guardarMonto(e.target.value)}
                                />
                            </div>

                            <button 
                                type="submit"
                                className="btn btn-primary font-weight-bold text-uppercase d-block w-100"
                            >Agregar</button>
                        </form>

                        { cargando ? <p>Cargando...</p> : null }
                        
                        { error ? <p className="alert alert-danger p2 mt-4 text-center">Hubo un error</p> : null }
                    </div>
                </div>
            </div>
        </div>
     );
}
 
export default NuevaTiendaUsuario;