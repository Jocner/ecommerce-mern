import {
    AGREGAR_TIENDA,
    AGREGAR_TIENDA_EXITO,
    AGREGAR_TIENDA_ERROR,
    COMENZAR_DESCARGA_TIENDAS,
    DESCARGA_TIENDAS_EXITO,
    DESCARGA_TIENDAS_ERROR, 
    // OBTENER_PRODUCTO_ELIMINAR,
    // PRODUCTO_ELIMINADO_EXITO,
    // PRODUCTO_ELIMINADO_ERROR,
    // OBTENER_PRODUCTO_EDITAR,
    // COMENZAR_EDICION_PRODUCTO,
    // PRODUCTO_EDITADO_EXITO,
    // PRODUCTO_EDITADO_ERROR
} from '../types';
import clienteAxios from '../config/axios';
import Swal from 'sweetalert2';

// Crear nuevo registro para la tienda 
export function crearTiendaUsuarioAction(tiendas) {
    return async (dispatch) => {
        dispatch( agregarTienda() );

        try {
            // insertar en la API
            await clienteAxios.post('/api/nuevo', tiendas);

            // Si todo sale bien, actualizar el state
           dispatch( agregarTiendaExito(tiendas) );

            // Alerta
            Swal.fire(
                'Correcto', 
                'La informacion se ha guardado',
                'success'
            );

        } catch (error) {
            console.log(error);
            // si hay un error cambiar el state
            dispatch( agregarTiendaError(true) );

            // alerta de error
            Swal.fire({
                icon: 'error',
                title: 'Hubo un error',
                text: 'Hubo un error, intenta de nuevo'
            })
        }
    }
}

const agregarTienda = () => ({
    type: AGREGAR_TIENDA,
    payload: true
});

// si la informacion de la tienda se guarda en la base de datos
const agregarTiendaExito = tienda => ({
    type: AGREGAR_TIENDA_EXITO,
    payload: tienda
})

// si hubo un error
const agregarTiendaError = estado => ({
    type: AGREGAR_TIENDA_ERROR,
    payload: estado
});


// Función que descarga los productos de la base de datos
export function obtenerTiendasAction() {
    return async (dispatch) => {
        dispatch( descargarTiendas() );

        try {
            const respuesta = await clienteAxios.get('/api/consulta');
            dispatch( descargaTiendasExitosa(respuesta.data) )
        } catch (error) {
            console.log(error);
            dispatch( descargaTiendasError() )
        }
    }
}

const descargarTiendas = () => ({
    type: COMENZAR_DESCARGA_TIENDAS,
    payload: true
});

const descargaTiendasExitosa = productos => ({
    type: DESCARGA_TIENDAS_EXITO,
    payload: productos
})
const descargaTiendasError = () => ({
    type: DESCARGA_TIENDAS_ERROR, 
    payload: true
});

// // Selecciona y elimina el producto
// export function borrarProductoAction(_id) {
//     return async (dispatch) => {
//         dispatch(obtenerProductoEliminar(_id) );

//         try {
//             await clienteAxios.post(`/api/agregar/`);
//             dispatch( eliminarProductoExito() );

//             // Si se elimina, mostrar alerta
//             Swal.fire(
//                 'Eliminado',
//                 'El producto se eliminó correctamente',
//                 'success'
//             )
//         } catch (error) {
//             console.log(error);
//             dispatch( eliminarProductoError() );
//         }
//     }
// }

// const obtenerProductoEliminar = id => ({
//     type: OBTENER_PRODUCTO_ELIMINAR,
//     payload: id
// });
// const eliminarProductoExito = () => ({
//     type: PRODUCTO_ELIMINADO_EXITO
// })
// const eliminarProductoError = () => ({
//     type: PRODUCTO_ELIMINADO_ERROR,
//     payload: true
// });

// // Colocar producto en edición
// export function obtenerProductoEditar(producto) {
//     return (dispatch) => {
//         dispatch( obtenerProductoEditarAction(producto) )
//     }
// }

// const obtenerProductoEditarAction = producto => ({
//     type: OBTENER_PRODUCTO_EDITAR,
//     payload: producto
// })

// // Edita un registro en la api y state
// export function editarProductoAction(producto) {
//     return async (dispatch) => {
//         dispatch( editarProducto() );

//         try {
//             await clienteAxios.put(`/productos/${producto.id}`, producto);    
//             dispatch( editarProductoExito(producto) );
//         } catch (error) {
//             console.log(error);
//             dispatch( editarProductoError() );
//         }
//     }
// }
// const editarProducto = () => ({
//     type: COMENZAR_EDICION_PRODUCTO
// });

// const editarProductoExito = producto => ({
//     type: PRODUCTO_EDITADO_EXITO,
//     payload: producto
// });

// const editarProductoError = () => ({
//     type: PRODUCTO_EDITADO_ERROR,
//     payload: true
// })