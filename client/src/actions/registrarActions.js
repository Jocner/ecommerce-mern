import {
    REGISTRO_USUARIO,
    REGISTRO_USUARIO_EXITO,
    REGISTRO_USUARIO_ERROR,
} from '../types';
import clienteAxios from '../config/axios';
import Swal from 'sweetalert2';

// Crear nuevos usuarios
export function registrarUsuarioAction(usuario) {
    return async (dispatch) => {
        dispatch( agregarUsuario() );

        try {
            // insertar en la API
            await clienteAxios.post('/api/usuario', usuario);

            // Si todo sale bien, actualizar el state
           dispatch( agregarUsuarioExito(usuario) );

            // Alerta
            Swal.fire(
                'Correcto', 
                'El usuario se agregó correctamente',
                'success'
            );

        } catch (error) {
            console.log(error);
            // si hay un error cambiar el state
            dispatch( agregarUsuarioError(true) );

            // alerta de error
            Swal.fire({
                icon: 'error',
                title: 'Hubo un error',
                text: 'Hubo un error, intenta de nuevo'
            })
        }
    }
}

const agregarUsuario = () => ({
    type: REGISTRO_USUARIO,
    payload: true
});

// si el producto se guarda en la base de datos
const agregarUsuarioExito = usuario => ({
    type: REGISTRO_USUARIO_EXITO,
    payload: usuario
});

// si hubo un error
const agregarUsuarioError = estado => ({
    type: REGISTRO_USUARIO_ERROR,
    payload: estado
});


